/*
 * Copyright 2017 Marc Nause <marc.nause@gmx.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see  http:// www.gnu.org/licenses/.
 */
package de.audioattack.openlink.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Handles sharing of selected text in incognito mode.
 */
public class IncognitoActivity extends Activity {

    @Override
    protected final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get intent, action and MIME type
        final Intent intent = getIntent();
        final String action = intent.getAction();
        final String type = intent.getType();

        if (type != null && Intent.ACTION_SEND.equals(action)) {
            final Intent newIntent = new Intent(getApplicationContext(), MenuActivity.class);
            newIntent.setAction(action);
            newIntent.setType(type);
            final Bundle extras = intent.getExtras();
            if (extras != null) {
                extras.putString(Intent.EXTRA_TEXT, intent.getStringExtra(Intent.EXTRA_TEXT));
                extras.putBoolean(MenuActivity.EXTRA_BOOLEAN_INCOGNITO, true);
                newIntent.putExtras(extras);
            }
            startActivity(newIntent);

        } else {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }

        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        setVisible(true);
    }

}
